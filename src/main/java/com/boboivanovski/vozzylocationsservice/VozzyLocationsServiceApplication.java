package com.boboivanovski.vozzylocationsservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VozzyLocationsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(VozzyLocationsServiceApplication.class, args);
	}

}
