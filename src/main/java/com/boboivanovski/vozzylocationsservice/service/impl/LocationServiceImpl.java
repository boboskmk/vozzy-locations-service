package com.boboivanovski.vozzylocationsservice.service.impl;

import org.springframework.stereotype.Service;

import com.boboivanovski.vozzylocationsservice.model.LatLng;
import com.boboivanovski.vozzylocationsservice.model.api.request.AddLocationRequest;
import com.boboivanovski.vozzylocationsservice.model.api.request.UpdateLocationRequest;
import com.boboivanovski.vozzylocationsservice.model.api.response.GenericResponse;
import com.boboivanovski.vozzylocationsservice.model.api.response.GetLocationsInRadiusResponse;
import com.boboivanovski.vozzylocationsservice.service.LocationService;

@Service
public class LocationServiceImpl implements LocationService  {

	@Override
	public GenericResponse addLocation(AddLocationRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericResponse addLike(UpdateLocationRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericResponse removeLike(UpdateLocationRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericResponse addDislike(UpdateLocationRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericResponse removeDislike(UpdateLocationRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericResponse addYesVote(UpdateLocationRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericResponse removeYesVote(UpdateLocationRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericResponse addNoVote(UpdateLocationRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericResponse removeNoVote(UpdateLocationRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetLocationsInRadiusResponse getLocationsInRadius(LatLng position, int radius) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
