package com.boboivanovski.vozzylocationsservice.service;

import com.boboivanovski.vozzylocationsservice.model.LatLng;
import com.boboivanovski.vozzylocationsservice.model.api.request.AddLocationRequest;
import com.boboivanovski.vozzylocationsservice.model.api.request.UpdateLocationRequest;
import com.boboivanovski.vozzylocationsservice.model.api.response.GenericResponse;
import com.boboivanovski.vozzylocationsservice.model.api.response.GetLocationsInRadiusResponse;

public interface LocationService {
	
	GenericResponse addLocation(AddLocationRequest request);
	
	GenericResponse addLike(UpdateLocationRequest request);
	
	GenericResponse removeLike(UpdateLocationRequest request);
	
	GenericResponse addDislike(UpdateLocationRequest request);
	
	GenericResponse removeDislike(UpdateLocationRequest request);
	
	GenericResponse addYesVote(UpdateLocationRequest request);
	
	GenericResponse removeYesVote(UpdateLocationRequest request);
	
	GenericResponse addNoVote(UpdateLocationRequest request);
	
	GenericResponse removeNoVote(UpdateLocationRequest request);
	
	GetLocationsInRadiusResponse getLocationsInRadius(LatLng position, int radius); 

}
