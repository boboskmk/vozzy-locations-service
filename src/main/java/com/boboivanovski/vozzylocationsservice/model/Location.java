package com.boboivanovski.vozzylocationsservice.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "location")
public class Location {
	
	@Id
	private String id;
	
	private MarkerType markerType;
	
	@GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
	GeoJsonPoint position;
	
	// city
	
	// native city
	
	// address
	
	// native address
	
	private long createTimestamp;
	 
	private long scheduledDeleteTimestamp;
	
	// list of liked ids
	private List<String> listOfWhoLikedTheMarker;
	
	// list of disliked ids
	private List<String> listOfWhoDisikedTheMarker;
	
	// list of yes votes
	private List<String> listOfWhoVotedToExtendTheTime;
	
	// lost of no votes 
	private List<String> listOfWhoVotedToNotExtendTheTime;
	
	
	public Location() {
		
	}

	public Location(String id, MarkerType markerType, GeoJsonPoint position, long createTimestamp, long scheduledDeleteTimestamp) {
		this.id = id;
		this.markerType = markerType;
		this.position = position;
		this.createTimestamp = createTimestamp;
		this.scheduledDeleteTimestamp = scheduledDeleteTimestamp;
	}




	public Location(MarkerType markerType, GeoJsonPoint position, long createTimestamp, long scheduledDeleteTimestamp,
			List<String> listOfWhoLikedTheMarker, List<String> listOfWhoDisikedTheMarker,
			List<String> listOfWhoVotedToExtendTheTime, List<String> listOfWhoVotedToNotExtendTheTime) {
		this.markerType = markerType;
		this.position = position;
		this.createTimestamp = createTimestamp;
		this.scheduledDeleteTimestamp = scheduledDeleteTimestamp;
		this.listOfWhoLikedTheMarker = listOfWhoLikedTheMarker;
		this.listOfWhoDisikedTheMarker = listOfWhoDisikedTheMarker;
		this.listOfWhoVotedToExtendTheTime = listOfWhoVotedToExtendTheTime;
		this.listOfWhoVotedToNotExtendTheTime = listOfWhoVotedToNotExtendTheTime;
	}

	
	public String getId() {
		return id;
	}

	public MarkerType getMarkerType() {
		return markerType;
	}

	public void setMarkerType(MarkerType markerType) {
		this.markerType = markerType;
	}

	public GeoJsonPoint getPosition() {
		return position;
	}
	
	public LatLng getLatLng() {
		return new LatLng(position.getX(), position.getY());
	}
 
	public void setPosition(GeoJsonPoint position) {
		this.position = position;
	}

	public long getCreateTimestamp() {
		return createTimestamp;
	}

	public void setCreateTimestamp(long createTimestamp) {
		this.createTimestamp = createTimestamp;
	}

	public long getScheduledDeleteTimestamp() {
		return scheduledDeleteTimestamp;
	}

	public void setScheduledDeleteTimestamp(long scheduledDeleteTimestamp) {
		this.scheduledDeleteTimestamp = scheduledDeleteTimestamp;
	}

	public List<String> getListOfWhoLikedTheMarker() {
		return listOfWhoLikedTheMarker;
	}

	public void setListOfWhoLikedTheMarker(List<String> listOfWhoLikedTheMarker) {
		this.listOfWhoLikedTheMarker = listOfWhoLikedTheMarker;
	}

	public List<String> getListOfWhoDisikedTheMarker() {
		return listOfWhoDisikedTheMarker;
	}

	public void setListOfWhoDisikedTheMarker(List<String> listOfWhoDisikedTheMarker) {
		this.listOfWhoDisikedTheMarker = listOfWhoDisikedTheMarker;
	}

	public List<String> getListOfWhoVotedToExtendTheTime() {
		return listOfWhoVotedToExtendTheTime;
	}

	public void setListOfWhoVotedToExtendTheTime(List<String> listOfWhoVotedToExtendTheTime) {
		this.listOfWhoVotedToExtendTheTime = listOfWhoVotedToExtendTheTime;
	}

	public List<String> getListOfWhoVotedToNotExtendTheTime() {
		return listOfWhoVotedToNotExtendTheTime;
	}

	public void setListOfWhoVotedToNotExtendTheTime(List<String> listOfWhoVotedToNotExtendTheTime) {
		this.listOfWhoVotedToNotExtendTheTime = listOfWhoVotedToNotExtendTheTime;
	}
	
	
	
	

}
