package com.boboivanovski.vozzylocationsservice.model.enums;

public enum MarkerTypes {
	
    POLICE(0), RIOT(1), ACCIDENT(2), WORK_ON_ROAD(3);

    private final Integer value;

    MarkerTypes(final int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
