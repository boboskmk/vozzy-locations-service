package com.boboivanovski.vozzylocationsservice.model.enums;

public enum Roles {

    ADMIN(0), CUSTOMER(1);

    private final Integer value;

    Roles(final int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
