package com.boboivanovski.vozzylocationsservice.model.api.response;

import java.util.List;

import com.boboivanovski.vozzylocationsservice.model.Location;

public class GetLocationsInRadiusResponse {

	private List<Location> locations;

	public GetLocationsInRadiusResponse() {
		
	}
	
	public GetLocationsInRadiusResponse(List<Location> locations) {
		this.locations = locations;
	}

	public List<Location> getLocations() {
		return locations;
	}

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}
	
	
	
}
