package com.boboivanovski.vozzylocationsservice.model.api.request;

import com.boboivanovski.vozzylocationsservice.model.LatLng;
import com.boboivanovski.vozzylocationsservice.model.MarkerType;

public class AddLocationRequest {

	private LatLng position;
	
	private MarkerType markerType;
	
	private String userId;
	
	public AddLocationRequest() {
		
	}

	public AddLocationRequest(LatLng position, MarkerType markerType, String userId) {
		this.position = position;
		this.markerType = markerType;
		this.userId = userId;
	}

	public LatLng getPosition() {
		return position;
	}

	public MarkerType getMarkerType() {
		return markerType;
	}

	public String getUserId() {
		return userId;
	}
	
	
	
}
