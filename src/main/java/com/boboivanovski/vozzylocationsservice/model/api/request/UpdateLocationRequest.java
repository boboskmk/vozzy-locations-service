package com.boboivanovski.vozzylocationsservice.model.api.request;

public class UpdateLocationRequest {

	private String locationId;
	
	private String userId;
	
	public UpdateLocationRequest() {
		
	}

	public UpdateLocationRequest(String locationId, String userId) {
		this.locationId = locationId;
		this.userId = userId;
	}

	public String getLocationId() {
		return locationId;
	}

	public String getUserId() {
		return userId;
	}
	
	
	
}
