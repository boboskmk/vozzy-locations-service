package com.boboivanovski.vozzylocationsservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.boboivanovski.vozzylocationsservice.model.LatLng;
import com.boboivanovski.vozzylocationsservice.model.api.request.AddLocationRequest;
import com.boboivanovski.vozzylocationsservice.model.api.request.UpdateLocationRequest;
import com.boboivanovski.vozzylocationsservice.model.api.response.GenericResponse;
import com.boboivanovski.vozzylocationsservice.model.api.response.GetLocationsInRadiusResponse;
import com.boboivanovski.vozzylocationsservice.service.LocationService;

@RestController
public class LocationController {
	
	@Autowired
	private LocationService locationService;

	
	@PostMapping("/location")
	public ResponseEntity<GenericResponse> addLocation(@RequestBody AddLocationRequest request) {
		return ResponseEntity.ok(locationService.addLocation(request));
	}
	
	@PutMapping("/location/like")
	public ResponseEntity<GenericResponse> addLike(@RequestBody UpdateLocationRequest request) {
		return ResponseEntity.ok(locationService.addLike(request));
	}
	
	@DeleteMapping("/location/like")
	public ResponseEntity<GenericResponse> removeLike(@RequestBody UpdateLocationRequest request) {
		return ResponseEntity.ok(locationService.removeLike(request));
	}
	
	@PutMapping("/location/dislike")
	public ResponseEntity<GenericResponse> addDisike(@RequestBody UpdateLocationRequest request) {
		return ResponseEntity.ok(locationService.addDislike(request));
	}
	
	@DeleteMapping("/location/dislike")
	public ResponseEntity<GenericResponse> removeDisike(@RequestBody UpdateLocationRequest request) {
		return ResponseEntity.ok(locationService.removeDislike(request));
	}
	
	@PutMapping("/location/vote-yes")
	public ResponseEntity<GenericResponse> addYesVote(@RequestBody UpdateLocationRequest request) {
		return ResponseEntity.ok(locationService.addYesVote(request));
	}
	
	@DeleteMapping("/location/vote-yes")
	public ResponseEntity<GenericResponse> removeYesVote(@RequestBody UpdateLocationRequest request) {
		return ResponseEntity.ok(locationService.removeYesVote(request));
	}
	
	@PutMapping("/location/vote-no")
	public ResponseEntity<GenericResponse> addNoVote(@RequestBody UpdateLocationRequest request) {
		return ResponseEntity.ok(locationService.addNoVote(request));
	}
	
	@DeleteMapping("/location/vote-no")
	public ResponseEntity<GenericResponse> removeNoVote(@RequestBody UpdateLocationRequest request) {
		return ResponseEntity.ok(locationService.removeNoVote(request));
	}
	
	@GetMapping("/location")
	public ResponseEntity<GetLocationsInRadiusResponse> getLocationsInRadius(@RequestParam(name = "lat") double latitude,
			@RequestParam(name = "lng") double longitude, @RequestParam int radius) {
		
		//	TODO:	if (radius <= 0) throw new BadRequestExcpetion();
		LatLng position = new  LatLng(latitude, longitude);
		
		return ResponseEntity.ok(locationService.getLocationsInRadius(position, radius));
	}

	
}
