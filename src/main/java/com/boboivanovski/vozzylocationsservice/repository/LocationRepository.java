package com.boboivanovski.vozzylocationsservice.repository;

import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.boboivanovski.vozzylocationsservice.model.Location;
import com.mongodb.client.model.geojson.Point;

public interface LocationRepository  extends MongoRepository<Location, String> {

    GeoResults<Location> findByLocationNear(Point p, Distance d);

}
